# MIBiG SMILES

## mibig_smiles.py

This script reads the dump file with all the entries of the MIBiG repository and
tries to retrieve all the SMILES strings (which store the chemical structure of the compound).

The dump can be downloaded [here](http://mibig.secondarymetabolites.org/download.html), 
and contains the individual information about each BGC entry in JSON format.

If the SMILES string is not readily available, it will try PubChem first (it has
most of the ids) through its [RESTful interface](https://pubchem.ncbi.nlm.nih.gov/pug_rest/PUG_REST_Tutorial.html) (so it needs the [requests](http://docs.python-requests.org/en/master/) package).
After that, it will also try ChemSpider (through [ChemSpiPy](https://github.com/mcs07/ChemSpiPy)), though in this case you will need a 
Security Token from them (which you get for free after signing for the service).
These two cover 99% of the database-deposited cases, so the remaining are only 
reported in the log (those would be chEBI and chEMBL). At the moment (dump 1.2) 
there are 8 such cases which are trivial to manually search for.

The script outputs several files:

* log.txt: Warnings and problems found, multihits from querying by name etc.,
 and a report of how many strings where found
* smiles.txt: For each BGC, the SMILES string of each of its compounds (one 
 compound per line)
* all_BGCs.txt: Results for each compound in each BGC (whether it was found, 
how etc.)
* found_with_info.txt: A subset of all_BGCs.txt. Only reports found strings.
* found_by_name.txt: List of compounds (by frequency) that where found by
searching by compound name.
* not_found.txt: List of compounds (by frequency) that could not be found by 
searching by compound name.
* no_info_BGC_files.txt: List of file names within the dump file with at least
 one compound with no information on chemical structure (neither SMILES string 
 nor database id, only compound name)
* ignored.txt: Entries with only compound name. These are generic compounds 
like 'polysaccharide'.

There are examples of this files when using the latest version of BIG 
database (1.2, december 24th, 2015).

This scripts runs on Pyhton 3

#! /usr/bin/env/ python
# coding: utf8

# updated around 2016-09-12

import json  # for parsing json files
import glob  # for traversing the directory in search of json files
import requests  # for querying for info on PubChem's RESTful interface
import time  # PubChem's documentation asks for no more than 5 requests/sec, so a delay is necessary
from chemspipy import ChemSpider
import operator
from rdkit import Chem  # comment if RDKit is not available. Also check the annotate_smiles function
import argparse

"""Small script to analyze BGC files from MIBiG (mibig.secondarymetabolites.org) and extract their SMILES (chemical
structure in string format) data.


Complete rewrite.
Will also write compound names in the files.
Integrated with missing_smiles.py which was a script exclusively for querying by compound name

Pseudo algorithm:
Open all the BGCXXXXXXX.json files (if there are any). For each file:
    Look for general_params -> compounds
    If the key chem_struct is found, annotate SMILES string
    If not, try the database_deposited key. If found:
        Try PubChem, then ChemSpider, then chEBI, then chEMBL
    else, try querying PubChem, then ChemSpider for compound name

How to use:
- Download and decompress the latest json file with entries from mibig.secondarymetabolites.org/download.html
- Execute this python script within that directory.

External stuff:
- [requests package for HTTP requests](http://requests.readthedocs.org/en/latest/user/quickstart/)
- [PubChem's RESTful interface documentation](https://pubchem.ncbi.nlm.nih.gov/pug_rest/PUG_REST_Tutorial.html)
- [ChemSpiPy for interaction with ChemSpider](http://chemspipy.readthedocs.org/en/latest/guide/install.html). Also, [this post](http://blog.matt-swain.com/post/16893587098/chemspipy-a-python-wrapper-for-the-chemspider)


TO DO:
- Clean up all variables' names
- Fix check for ChemSpider's Security Token (cannot catch the exception)
x Canonicalize SMILES strings before storing



jorge.navarromunoz@wur.nl
"""
log = open("log.txt", "w")

parser = argparse.ArgumentParser()
parser.add_argument("-j", "--jsondir", help="Enter the place where the json files are", type=str, required = True)
args = parser.parse_args()


# get a list of all json files
data = {}
json_path = args.jsondir
files_processed = 0
for item in glob.glob(json_path + "*.json"):
    # do minimal validation:
    file_name = item[-15:] #remove path
    if file_name[:3] == "BGC":
        try:
            data[file_name[:-5]] = json.load(open(json_path + file_name))
        except ValueError as e:
            print("Error parsing " + file_name + " (" + str(e) + ")")
        else:
            files_processed += 1
    else:
        print("Invalid pattern in json file: " + file_name)
        log.write("Invalid filename: " + file + "\n")

print("")

if files_processed == 0:
    log.write("No json files found")
    log.close()
    quit("No json files found")


# open files, prepare variables etc.
smiles_file = open("smiles.txt", "w")
no_info_BGC_files = open("no_info_BGC_files.txt", "w")
no_info_files_list = []
found_info = open("found_with_info.txt", "w")
ignored = open("ignored.txt", "w")
all_bgcs = open("all_BGCs.txt", "w")  # everything reported on BGCs in the terminal (so, found_info is a subset of this)

# "glycoconjugates present on bacterial cell surfaces" http://www.hindawi.com/journals/ijmicro/2011/127870/
# when searching by compound name, we can ignore these
blacklist = ["polysaccharide", "lipopolysaccharide", "O-antigen", "capsular polysaccharide", "exopolysaccharide",
             "glycopeptidolipid", "S-layer glycan", "lipo-oligosaccharide"]

pubchem_cid_list = ""  # store BGC key and CIDs for log file
chemspider_cid_list = ""
chebi_cid_list = ""
chembl_cid_list = ""

bgc = {}  # list of compound names for each bgc
compound_db = {}  # which BGC stores a given compound
smiles_db = {}
# smiles = {}  # ALL the SMILES will eventually end up here

has_smiles = False
has_smiles_in_db = False
has_no_smiles = False

direct_smiles = 0
smiles_in_db = 0
no_info = 0

files_direct_smiles = 0
files_smiles_in_db = 0
files_no_info = 0

pubchem = 0
chemspider = 0
chebi = 0
chembl = 0

hits = 0
compounds_found = {}
compounds_not_found = {}
compounds_ignored = {}  # these are on the blacklist
last_resort_compounds = []  # try already annotated compounds. Last resort. format: "bgc_key:compound_name"
multihits_pubchem = "Compounds with more than one hit, PubChem:\n"
multihits_chemspider = "Compounds with more than one hit, ChemSpider:\n"
spider_results = []


# returns canonical SMILES
def annotate_smiles(bgc_key, compound_name, compound_smile):
    # if it's already there, check for possible missannotations
    global compound_db, smiles_db

    # If RDKit is not installed, uncomment following line..
    # canonical = compound_smile
    # ...end comment the following...
    # catching errors in SMILES. See https://github.com/rdkit/rdkit/issues/642
    molecule = Chem.MolFromSmiles(compound_smile, sanitize=False)
    try:
        Chem.SanitizeMol(molecule)
    except ValueError as e:
        print("\tRDKit ERROR: " + str(e))
        log.write(bgc_key + ":" + compound_name+" SMILES string has ERRORS (will not keep it). RDKit output: " + str(e) + "\n")
        return ""
    else:
        canonical = Chem.MolToSmiles(molecule,True)
    # ...and put the following one level up
        if compound_name in compound_db:
            already_has_smile = False
            for bgc in compound_db[compound_name]:
                if smiles_db[bgc, compound_name] == canonical:  # this should be canonicalized first
                    already_has_smile = True

            compound_db[compound_name].append(bgc_key)
            smiles_db[bgc_key, compound_name] = canonical

            # no match against any of the smiles in database
            if not already_has_smile:
                print("Mismatch for compound " + compound_name)
                log.write("Mismatch for compound " + compound_name + "\n")

                for bgc in compound_db[compound_name]:
                    print("\t" + bgc + ": " + smiles_db[bgc, compound_name])
                    log.write("\t" + bgc + ": " + smiles_db[bgc, compound_name] + "\n")
        else:
            compound_db[compound_name] = [bgc_key]
            smiles_db[bgc_key, compound_name] = canonical  # canonicalize first...

        return canonical


decision = input("Report PubChem IDs in log file? (blank = yes): ")
log_pubchem = True if decision == "" else False
decision = input("Report ChemSpider IDs in log file? (blank = yes): ")
log_chemspider = True if decision == "" else False
# For info on ChemSpider w/python see
# http://blog.matt-swain.com/post/16893587098/chemspipy-a-python-wrapper-for-the-chemspider
print("\nConnection to ChemSpider needs a Security Token.")
print("You can put your key in a file called chemspiderkey")
security_token = ""
try:
    chemspiderkeyfile = open("chemspiderkey", "r")
except:
    security_token = input("Type it now to try connection or leave it blank to skip it: ")
    spider = ChemSpider(security_token)
else:
    print("  File with ChemSpider key found. Using it...")
    security_token = chemspiderkeyfile.readline().strip()
    spider = ChemSpider(security_token)

print("\n Analyzing MIBiG entries:")
for bgc_key in sorted(data):
    bgc[bgc_key] = []

    compounds_list = data[bgc_key]["general_params"]["compounds"]

    has_smiles = False
    has_smiles_in_db = False
    has_no_smiles = False

    for compound in compounds_list:
        compound_name = compound["compound"]

        if compound_name != compound_name.strip():
            print("\tWarning: Leading or trailing spaces detected: |" + compound_name + "|. Fixing...")
            log.write("Warning: Leading or trailing spaces detected: |" + compound_name + "|. Fixing...\n")
            compound_name = compound_name.strip()
            
        bgc[bgc_key].append(compound_name)


        bgc_msj = bgc_key + ":[" + compound_name + "] (" + ",".join(
            map(str, data[bgc_key]["general_params"]["biosyn_class"])) + ") "

        if "chem_struct" in compound:
            has_smiles = True
            direct_smiles += 1

            canonical_smile = annotate_smiles(bgc_key, compound_name, compound["chem_struct"])
            if canonical_smile != "":
                smiles_file.write(bgc_key + ":[" + compound_name + "]:" + canonical_smile + "\n")

            print(bgc_msj + "Found in JSON file")
            found_info.write(bgc_msj + "Found in JSON file\n")
            all_bgcs.write(bgc_msj + "Found in JSON file\n")

        # aparently, if the "database_deposited" entry is present, it's
        #   always set to True so no additional checks
        # We try databases in the order PubChem -> ChemSpider -> chEBI -> chEMBL.
        #   First two cover about 99%
        elif "database_deposited" in compound:
            has_smiles_in_db = True
            smiles_in_db += 1

            if "PubChem" in compound["databases_deposited"]:
                pubchem += 1

                cid = str(compound["pubchem_id"])

                pubchem_cid_list += bgc_key + "\t" + "[" + compound_name + "] " + cid + "\n"

                url = "http://pubchem.ncbi.nlm.nih.gov/rest/pug/compound/cid/" + cid + "/property/IsomericSMILES/TXT"
                req = requests.get(url)

                # 200: success
                if req.status_code == 200:
                    compound_smile = req.text.strip()

                    canonical_smile = annotate_smiles(bgc_key, compound_name, compound_smile)
                    if canonical_smile != "":
                        smiles_file.write(bgc_key + ":[" + compound_name + "]:" + canonical_smile + "\n")

                    print(bgc_msj + "PubChem (CID " + cid + ")")
                    found_info.write(bgc_msj + "PubChem (CID " + cid + "))\n")
                    all_bgcs.write(bgc_msj + "PubChem (CID " + cid + "))\n")

                # probably a malformed url
                elif req.status_code == 400:
                    print(
                        "status 400: 'Request is improperly formed' (prob. bad url?). BGC: " + bgc_key + ", CID: " + cid)
                    log.write(
                        "status 400: 'Request is improperly formed' (prob. bad CID?). BGC: " + bgc_key + ", CID: " + cid + "\n")
                    all_bgcs.write(bgc_msj + "Error PubChem (status 400)\n")

                elif req.status_code == 404:
                    print("status 404 (not found)" + bgc_key + ", CID: " + cid)
                    log.write(
                        "status 404: 'Not found' (prob. bad CID?). BGC: " + bgc_key + ", CID: " + cid + "\n")
                    all_bgcs.write(bgc_msj + "Error PubChem (status 404)\n")

                else:
                    print("Request to PubChem went wrong. BGC: " + bgc_key + ",CID=" + cid + ", status: " + str(
                        req.status_code))
                    print(url)
                    all_bgcs.write(bgc_msj + "Error PubChem: " + str(req.status_code) + "\n")

                time.sleep(.21)

            elif "ChemSpider" in compound["databases_deposited"]:
                chemspider += 1

                cid = compound["chemspider_id"]

                chemspider_cid_list += bgc_key + "\t" + "[" + compound_name + "] " + str(cid) + "\n"

                if security_token != "":
                    spider_compound = spider.get_compound(cid)

                    canonical_smile = annotate_smiles(bgc_key, compound_name, spider_compound.smiles)
                    if canonical_smile != "":
                        smiles_file.write(
                        bgc_key + ":" + "[" + compound_name + "]:" + canonical_smile + "\n")

                    print(bgc_msj + "ChemSpider (CID " + str(cid) + "))")
                    found_info.write(bgc_msj + "ChemSpider (CID " + str(cid) + "))\n")
                    all_bgcs.write(bgc_msj + "ChemSpider (CID " + str(cid) + "))\n")
                else:
                    print(bgc_msj + "ChemSpider (CID). NOT retrieved (Security Token missing)")
                    found_info.write(bgc_msj + "ChemSpider (CID). NOT retrieved (Security Token missing)\n")
                    all_bgcs.write(bgc_msj + "ChemSpider (CID). NOT retrieved (Security Token missing)\n")

            elif "chEBI" in compound["databases_deposited"]:
                chebi += 1
                chebi_cid_list += bgc_key + "\t" + "[" + compound_name + "]" + str(compound["chebi_id"]) + "\n"

                print(bgc_msj + "chEBI")
                found_info.write(bgc_msj + "chEBI\n")
                all_bgcs.write(bgc_msj + "chEBI\n")

            elif "chEMBL" in compound["databases_deposited"]:
                chembl += 1
                chembl_cid_list += bgc_key + "\t" + "[" + compound_name + "]" + str(
                    compound["chembl_id"]) + "\n"

                print(bgc_msj + "chEMBL")
                found_info.write(bgc_msj + "chEMBL\n")
                all_bgcs.write(bgc_msj + "chEMBL\n")

            else:
                print("Unknown database in " + bgc_key + ": " + ",".join(
                    map(str, compound["databases_deposited"])))
                log.write("Unknown database " + bgc_key + ": " + ",".join(
                    map(str, compound["databases_deposited"])) + "\n")
                all_bgcs.write(
                    bgc_key + "Unknown database: " + ",".join(map(str, compound["databases_deposited"])) + "\n")

        else:
            has_no_smiles = True
            no_info += 1

            if bgc_key not in no_info_files_list:
                no_info_BGC_files.write(bgc_key + "\n")
                no_info_files_list.append(bgc_key)

            if compound_name in blacklist:
                print(bgc_msj + "BLACKLIST")
                ignored.write(bgc_msj + "\n")
                all_bgcs.write(bgc_msj + "BACKLIST\n")

            else:
                url = "http://pubchem.ncbi.nlm.nih.gov/rest/pug/compound/name/" + compound_name + "/cids/TXT"
                req = requests.get(url)
                response = req.text.strip()
                del spider_results[:]

                # Codes: https://pubchem.ncbi.nlm.nih.gov/pug_rest/PUG_REST.html#_Toc409516765
                # 503   Services unavailable (unable to service request due to maintenance downtime or capacity problems. Try later)
                # 200:  success
                # 400:  Bad request
                # 404:  not found
                if req.status_code == 200:
                    hit_order = response.count("\n") + 1

                    if hit_order == 1:
                        hits += 1

                        # now we're ready to get the SMILES
                        url = "http://pubchem.ncbi.nlm.nih.gov/rest/pug/compound/cid/" + str(
                            response) + "/property/IsomericSMILES/TXT"
                        req = requests.get(url)
                        compound_smile = req.text.strip()

                        canonical_smile = annotate_smiles(bgc_key, compound_name, compound_smile)
                        if canonical_smile != "":
                            smiles_file.write(bgc_key + ":[" + compound_name + "]:" + canonical_smile + "\n")

                        if compound_name in compounds_found:
                            compounds_found[compound_name] += 1
                        else:
                            compounds_found[compound_name] = 1

                        print(bgc_msj + "Pubchem (compound name) " + response)
                        found_info.write(bgc_msj + "PubChem (compound name) " + response + "\n")
                        all_bgcs.write(bgc_msj + "PubChem (compound name) " + response + "\n")

                    # multiple hits; try ChemSpider for exact compound
                    else:
                        if security_token != "":
                            r = spider.search(
                                "'" + compound_name + "'")  # TIP: using double quotes or no quotes are all is prone to return bogus hits
                            for result in r:
                                spider_results.append(
                                    result)  # note that spider.search returns a list of spider-Compounds

                            if len(spider_results) == 1 and r.message != "Found by splitting query string into separate tokens" and "approximate match" not in r.message:
                                # bingo!
                                hits += 1
                                spider_compound = spider_results[0]

                                if compound_name in compounds_found:
                                    compounds_found[compound_name] += 1
                                else:
                                    compounds_found[compound_name] = 1

                                canonical_smile = annotate_smiles(bgc_key, compound_name, spider_compound.smiles)
                                if canonical_smile != "":
                                    smiles_file.write(
                                    bgc_key + ":[" + compound_name + "]:" + canonical_smile + "\n")

                                print(bgc_msj + "ChemSpider (compound name) " + str(
                                    spider_compound.csid) + " SPIDER (" + r.message + ")")
                                found_info.write(bgc_msj + "ChemSpider (compound name) " + str(
                                    spider_compound.csid) + " SPIDER (" + r.message + ")\n")
                                all_bgcs.write(bgc_msj + "ChemSpider (compound name) " + str(
                                    spider_compound.csid) + " SPIDER (" + r.message + ")\n")

                            else:
                                multihits_pubchem += bgc_key + " (" + compound_name + "): [" + ",".join(
                                    map(str, data[bgc_key]["general_params"]["biosyn_class"])) \
                                                        + "], CIDs: " + response.replace("\n", " ") + "\n"

                                # multihits in both databases
                                if len(spider_results) > 1:
                                    multihits_chemspider += bgc_key + " (" + compound_name + "): [" + ",".join(
                                        map(str, data[bgc_key]["general_params"]["biosyn_class"])) \
                                                            + "], CIDs: " + " ".join(
                                        map(str, [x.csid for x in spider_results])) + "\n"

                                    print(bgc_msj + "Multihits in both PubChem and ChemSpider")
                                    all_bgcs.write(bgc_msj + "Multihits in both PubChem and ChemSpider\n")
                                else:
                                    print(bgc_msj + "Multihits in PubChem, not found in ChemSpider")
                                    all_bgcs.write(bgc_msj + "Multihits in PubChem, not found in ChemSpider\n")

                                if compound_name in compounds_not_found:
                                    compounds_not_found[compound_name] += 1
                                else:
                                    compounds_not_found[compound_name] = 1

                                last_resort_compounds.append(bgc_key + ":" + compound_name)
                        else:
                            print(
                                bgc_msj + "Multihits with PubChem, did not try ChemSpider (missing Security Token)")
                            all_bgcs.write(
                                bgc_msj + "Multihits with PubChem, did not try ChemSpider (missing Security Token)\n")
                elif req.status_code == 404:
                    if security_token != "":
                        r = spider.search("'" + compound_name + "'")

                        for result in r:
                            spider_results.append(result)

                        if len(spider_results) == 1 and r.message != "Found by splitting query string into separate tokens" and "approximate match" not in r.message:
                            # bingo!
                            hits += 1
                            spider_compound = spider_results[0]

                            if compound_name in compounds_found:
                                compounds_found[compound_name] += 1
                            else:
                                compounds_found[compound_name] = 1

                            canonical_smile = annotate_smiles(bgc_key, compound_name, spider_compound.smiles)
                            if canonical_smile != "":
                                smiles_file.write(
                                bgc_key + ":[" + compound_name + "]:" + canonical_smile + "\n")

                            print(bgc_msj + "ChemSpider (compound name) " + str(
                                spider_compound.csid) + " SPIDER (" + r.message + ")")
                            found_info.write(bgc_msj + "ChemSpider (compound name) " + str(
                                spider_compound.csid) + " SPIDER (" + r.message + ")\n")
                            all_bgcs.write(bgc_msj + "ChemSpider (compound name) " + str(
                                spider_compound.csid) + " SPIDER (" + r.message + ")\n")

                        else:
                            if len(spider_results) > 1:
                                multihits_chemspider += bgc_key + " (" + compound_name + "): [" + ",".join(
                                    map(str, data[bgc_key]["general_params"]["biosyn_class"])) \
                                                        + "], CIDs: " + " ".join(
                                    map(str, [x.csid for x in spider_results])) + "\n"
                                print(bgc_msj + "Multihits in ChemSpider, not found in PubChem")
                                all_bgcs.write(bgc_msj + "Multihits in ChemSpider, not found in PubChem\n")
                            else:
                                print(bgc_msj + "Not found anywhere")
                                all_bgcs.write(bgc_msj + "Not found anywhere\n")

                            if compound_name in compounds_not_found:
                                compounds_not_found[compound_name] += 1
                            else:
                                compounds_not_found[compound_name] = 1

                            last_resort_compounds.append(bgc_key + ":" + compound_name)
                    else:
                        print(
                            bgc_msj + "Compound name not found in PubChem, did not try ChemSpider (missing Security Token)")
                        all_bgcs.write(
                            bgc_msj + "Compound name not found in PubChem, did not try ChemSpider (missing Security Token)\n")

                else:
                    print(req.status_code + ": " + url)
                    all_bgcs.write(bgc_msj + req.status_code + ": " + url)

                time.sleep(.5)  # let's be nice with PubChem's servers

    """
    Note that a BGC file might have several compounds, and some of them might have
        their SMILES string readily available, while other compounds in the same file
        would only have it stored on a database (or not at all). That is: the variables
        below don't have to sum exactly to files_processed.
    """
    if has_smiles:
        files_direct_smiles += 1
    if has_smiles_in_db:
        files_smiles_in_db += 1
    if has_no_smiles:
        files_no_info += 1

print("")
print("Trying last resort for unfound compounds (see if they were directly annotated in other files)...")
for pair in last_resort_compounds:
    bgc_key = pair.split(":")[0]
    compound = pair.split(":")[1]

    if compound in compound_db:
        print("Found " + bgc_key + ":[" + compound + "]!")
        log.write("Found " + bgc_key + ":[" + compound + "]!")

        for bgc in compound_db[compound]:
            print("\t" + bgc + ": " + smiles_db[bgc, compound])
            log.write("\t" + bgc + ": " + smiles_db[bgc, compound] + "\n")

no_info_BGC_files.close()
smiles_file.close()
ignored.close()
all_bgcs.close()

# Report:

msj1 = "'compounds' entries with directly accessible SMILES strings: " + str(direct_smiles) + " (in " + str(
    files_direct_smiles) + " files)"
msj2 = "'compounds' entries with indirectly accessible SMILES strings (PubChem, ChemSpider, chEBI or chEMBLs): " + str(
    smiles_in_db) + " (in " + str(files_smiles_in_db) + " files)"
msj3 = "'compounds' entries with no info on chemical product: " + str(no_info) + " (in " + str(
    files_no_info) + " files. See the list in no_info_BGC_files.txt)"
msj4 = "\tOf these, " + str(
    hits) + " were indirectly retrived by searching for the compound name (unique hit in either PubChem or ChemSpider)"
msj5 = "\tSee more info on compounds searched by name on found.txt, not_found.txt and found_with_info.txt"
msj6 = "Total 'compounds' entries: " + str(direct_smiles + smiles_in_db + no_info)
msj7 = "Total files processed: " + str(files_processed)

print("")
print(msj1)
print(msj2)
print(msj3)
print(msj4)
print(msj5)
print(msj6)
print(msj7)

log.write("\n" + msj1 + "\n" + msj2 + "\n" + msj3 + "\n" + msj4 + "\n" + msj5 + "\n" + msj6 + "\n" + msj7 + "\n")

if log_pubchem:
    log.write("\nCIDs from PubChem:\n" + pubchem_cid_list + "\n")

if log_chemspider:
    log.write("\nCIDs from chem_struct:\n" + chemspider_cid_list + "\n")

log.write("\nCIDs from chEBI:\n" + chebi_cid_list + "\n")
log.write("\nCIDs from chEMBL:\n" + chembl_cid_list + "\n")

log.write("\n" + multihits_pubchem + "\n")
log.write("\n" + multihits_chemspider + "\n")

log.close()

# Report compound frequencies
# turning the dictionary into an ordered list with dict.value as key
# http://stackoverflow.com/questions/613183/sort-a-python-dictionary-by-value?lq=1
found = open("found_by_name.txt", "w")
sorted_freq = sorted(compounds_found.items(), key=operator.itemgetter(1), reverse=True)
for pair in sorted_freq:
    found.write(str(pair[1]) + "\t" + pair[0] + "\n")
found.close()

not_found = open("not_found.txt", "w")
sorted_freq = sorted(compounds_not_found.items(), key=operator.itemgetter(1), reverse=True)
for pair in sorted_freq:
    not_found.write(str(pair[1]) + "\t" + pair[0] + "\n")
not_found.close()

quit()
